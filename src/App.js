import React, { Component } from "react";
import "./App.css";

import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Home from "./Pages/Home";
import One from "./Pages/One";
import Two from "./Pages/Two";
import Three from "./Pages/Three";
import Four from "./Pages/Four";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <Link to="/">Home</Link> | &nbsp;
            <Link to="/one">Page One</Link> | &nbsp;
            <Link to="/two">Page Two</Link> | &nbsp;
            <Link to="/three">Page Three</Link> | &nbsp;
            <Link to="/four">Page Four</Link>
          </header>
          <div>
            <Route exact path="/" component={Home} />
            <Route path="/one" component={One} />
            <Route path="/two" component={Two} />
            <Route path="/three" component={Three} />
            <Route path="/four" component={Four} />
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
